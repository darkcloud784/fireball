/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.warhead.fireball;

import com.warhead.warriors.api.WarriorsManager;
import com.warhead.warriors.api.skill.Skill;

/**
 *
 * @author mrodriguez
 */
public class Fireball extends Skill {
    
    FireListener listener = new FireListener();

    @Override
    public void onEnable() {
        WarriorsManager.registerTriggerListener(this, listener);
    }

    @Override
    public void onDisable() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
