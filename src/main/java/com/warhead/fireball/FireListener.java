/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.warhead.fireball;

import com.warhead.warriors.api.SkillBindManager;
import com.warhead.warriors.api.trigger.PlayerLeftClickTrigger;
import com.warhead.warriors.api.trigger.TriggerListener;
import org.spout.api.entity.Player;
import org.spout.api.geo.World;
import org.spout.api.geo.discrete.Point;
import org.spout.vanilla.data.effect.store.GeneralEffects;


/**
 *
 * @author mrodriguez
 */
public class FireListener extends TriggerListener {

    public boolean LeftClickTrigger(PlayerLeftClickTrigger leftClick) {
        Player player = leftClick.getPlayer();
        World world = player.getWorld();
        Point point;
        
        if (SkillBindManager.get().isBound(leftClick.getPlayer(), "Fireball")) {
            point = player.getScene().getPosition();
            GeneralEffects.SHOOT_FIREBALL.play(player, point);

            return true;
        }
        return false;
    }
}
